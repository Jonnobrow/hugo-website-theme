/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
function toggleNavigation() {
  var x = document.getElementById("menu");
  if (x.className === "responsive") {
    document.getElementById("nav-toggle-symbol").classList.remove("fa-times");
    document.getElementById("nav-toggle-symbol").classList.add("fa-bars");
    x.className = "";
  } else {
    document.getElementById("nav-toggle-symbol").classList.add("fa-times");
    document.getElementById("nav-toggle-symbol").classList.remove("fa-bars");
    x.className = "responsive";
  }
} 


const colourModeSwitch = document.querySelector(".colour-mode-switch");
function toggleColourMode() {
    if (!colourModeSwitch.classList.contains("checked")) {
        document.documentElement.setAttribute('data-theme', 'dark');
        document.getElementById('colour-mode-switch-label').classList.add('fa-sun');
        document.getElementById('colour-mode-switch-label').classList.remove('fa-moon');
        localStorage.setItem('theme', 'dark'); //add this
        colourModeSwitch.classList.add("checked");
    } else {
        document.documentElement.setAttribute('data-theme', 'light');
        document.getElementById('colour-mode-switch-label').classList.remove('fa-sun');
        document.getElementById('colour-mode-switch-label').classList.add('fa-moon');
        localStorage.setItem('theme', 'light'); //add this
        colourModeSwitch.classList.remove("checked");
    }
}

const currentTheme = localStorage.getItem('theme') ? localStorage.getItem('theme') : null;
if (currentTheme) {
    document.documentElement.setAttribute('data-theme', currentTheme);
    if (currentTheme === 'dark') {
        colourModeSwitch.classList.add("checked");
        document.getElementById('colour-mode-switch-label').classList.add('fa-sun');
        document.getElementById('colour-mode-switch-label').classList.remove('fa-moon');
    } else {
        document.getElementById('colour-mode-switch-label').classList.remove('fa-sun');
        document.getElementById('colour-mode-switch-label').classList.add('fa-moon');
    }
} else {
    if (window.matchMedia('prefers-color-scheme: dark').matches) {
        localStorage.setItem('theme', 'dark'); //add this
        colourModeSwitch.classList.add("checked");
        document.getElementById('colour-mode-switch-label').classList.add('fa-sun');
        document.getElementById('colour-mode-switch-label').classList.remove('fa-moon')
    } else {
        localStorage.setItem('theme', 'light'); //add this
        document.getElementById('colour-mode-switch-label').classList.remove('fa-sun');
        document.getElementById('colour-mode-switch-label').classList.add('fa-moon');
    }
}
